Speel MP3 en WAV direct op Homey van URL's.
Handig voor het rechtstreeks afspelen van geluid vanaf UPnP-servers of andere netwerkbronnen (ook die misschien net iets te groot zijn).

MP3-bestanden groter dan 900 KB worden opgesplitst en direct na elkaar uitgevoerd, met minimale onderbrekingen.
Zo blijft de mp3 spelen!
Het huidige maximum is ingesteld op 15 MB, waarvoor in totaal 50 MB vrij geheugen nodig zou zijn.


WAV is momenteel nog max. 900 KB.