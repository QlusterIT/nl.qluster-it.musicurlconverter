Play MP3 and WAV directly on Homey from URLs.
Usefull for playing sound directly from UPnP servers or other network sources (that also might just be a bit to big).

MP3 files larger then 900 KB are split and executed right after another, with minimal interruptions.
This way the mp3 keeps playing!
Current max is set to 15 MB, which would require a total of 50 MB free memory.


WAV is currently still 900 KB max.